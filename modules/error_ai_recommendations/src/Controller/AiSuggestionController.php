<?php

namespace Drupal\error_ai_recommendations\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\ai\OperationType\Chat\ChatInput;
use Drupal\ai\OperationType\Chat\ChatMessage;
use Exception;

/**
 * Controller for handling AI suggestions.
 */
final class AiSuggestionController extends ControllerBase
{
    /**
     * Handles the incoming request and builds the response.
     */
    public function __invoke(Request $request): JsonResponse
    {
        $encoded = $request->getContent();
        if (empty($encoded)) {
            $this->logError('No encoded string provided');
            return $this->errorResponse('No encoded string provided');
        }

        $data = json_decode($encoded);
        if (json_last_error() !== JSON_ERROR_NONE) {
            $this->logError('Invalid JSON provided');
            return $this->errorResponse('Invalid JSON provided');
        }

        $codeLines = $data->code->content ?? '';
        $filePath = $data->code->file ?? '';
        $lineNumber = $data->code->line ?? '';
        $errorMessage = $data->error_msg ?? '';

        $responseFix = $this->getAiResponse($codeLines, $errorMessage, $lineNumber, $filePath);
        return new JsonResponse(['response' => $responseFix]);
    }

    /**
     * Retrieves the AI-generated response.
     */
    private function getAiResponse(string $codeLines, string $error, string $lineNumber, string $filePath): string
    {
        $defaults = \Drupal::service('ai.provider')->getDefaultProviderForOperationType('chat');

        if (empty($defaults['provider_id']) || empty($defaults['model_id'])) {
            throw new Exception('No default AI provider/model configured.');
        }

        $provider = \Drupal::service('ai.provider')->createInstance($defaults['provider_id']);

        $messages = new ChatInput([
            new ChatMessage('system', 'You are a Drupal pro developer. Fix the following error in the provided code. I am sharing the code, line number, and file path.'),
            new ChatMessage('user', "Error: $error\nLine Number: $lineNumber\nFile Path: $filePath\nCode:\n$codeLines")
        ]);

        $output = $provider->chat($messages, $defaults['model_id']);
        $responseText = $output->getNormalized()->getText();

        return $this->formatResponse($responseText);
    }

    /**
     * Logs an error message.
     */
    private function logError(string $message): void
    {
        \Drupal::logger('lnweb')->error($message);
    }

    /**
     * Returns a standardized error response.
     */
    private function errorResponse(string $message, int $statusCode = 400): JsonResponse
    {
        return new JsonResponse(['error' => $message], $statusCode);
    }

    /**
     * Formats the AI response as HTML.
     */
    private function formatResponse(string $responseText): string
    {
        $responseText = str_replace('```php', "<pre id='response-code-html'><code class='language-php'>", $responseText);
        return str_replace('```', '</code></pre>', $responseText);
    }
}
